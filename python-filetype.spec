%global _empty_manifest_terminate_build 0
Name:		python-filetype
Version:	1.2.0
Release:	1
Summary:	Infer file type and MIME type of any file/buffer. No external dependencies.
License:	MIT
URL:		https://github.com/h2non/filetype.py
Source0:	https://files.pythonhosted.org/packages/bb/29/745f7d30d47fe0f251d3ad3dc2978a23141917661998763bebb6da007eb1/filetype-1.2.0.tar.gz
BuildArch:	noarch


%description
Small and dependency free `Python`_ package to infer file type and MIME
type checking the `magic numbers`_ signature of a file or buffer.

This is a Python port from `filetype`_ Go package.

%package -n python3-filetype
Summary:	Infer file type and MIME type of any file/buffer. No external dependencies.
Provides:	python-filetype = %{version}-%{release}
BuildRequires:	python3-devel
BuildRequires:	python3-setuptools

%description -n python3-filetype
Small and dependency free `Python`_ package to infer file type and MIME
type checking the `magic numbers`_ signature of a file or buffer.

This is a Python port from `filetype`_ Go package.

%package help
Summary:	Development documents and examples for filetype
Provides:	python3-filetype-doc

%description help
Small and dependency free `Python`_ package to infer file type and MIME
type checking the `magic numbers`_ signature of a file or buffer.

This is a Python port from `filetype`_ Go package.

%prep
%autosetup -n filetype-%{version}

%build
%py3_build

%install
%py3_install
install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
if [ -d usr/lib ]; then
	find usr/lib -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/lib64 ]; then
	find usr/lib64 -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/bin ]; then
	find usr/bin -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/sbin ]; then
	find usr/sbin -type f -printf "/%h/%f\n" >> filelist.lst
fi
touch doclist.lst
if [ -d usr/share/man ]; then
	find usr/share/man -type f -printf "/%h/%f.gz\n" >> doclist.lst
fi
popd
mv %{buildroot}/filelist.lst .
mv %{buildroot}/doclist.lst .

%files -n python3-filetype -f filelist.lst
%dir %{python3_sitelib}/*

%files help -f doclist.lst
%{_docdir}/*

%changelog
* Tue Nov 15 2022 wangjunqi <wangjunqi@kylinos.cn> - 1.2.0-1
- Update package to version 1.2.0

* Wed Sep 14 2022 Qiao Jijun <qiaojijun@kylinos.cn> - 1.1.0-1
- Update to 1.1.0	

* Fri May 14 2021 Python_Bot <Python_Bot@openeuler.org> - 1.0.7-1
- Package Spec generated

